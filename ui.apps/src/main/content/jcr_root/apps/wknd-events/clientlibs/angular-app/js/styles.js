(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/styles.scss":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/styles.scss ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "\n@import url(\"https://fonts.googleapis.com/css?family=Asar|Source+Sans+Pro:400,600,700\");\n/* Google Font import */\nbody {\n  background-color: #ffffff;\n  font-family: \"Source Sans Pro\", sans-serif;\n  margin: 0;\n  padding: 0;\n  font-size: 1rem;\n  font-weight: 300;\n  text-align: left;\n  color: #080808;\n  line-height: 1.5;\n  line-height: 1.6;\n  letter-spacing: 0.3px;\n  padding-top: 100px; }\n@media only screen and (min-width: 161px) and (max-width: 767px) {\n    body {\n      padding-top: 80px; } }\nh1, h2, h3, h4 {\n  font-family: \"Source Sans Pro\", sans-serif; }\nh1 {\n  font-size: 3rem; }\nh2 {\n  font-size: 2.5rem; }\nh3 {\n  font-size: 2rem; }\nh4 {\n  font-size: 1.5rem; }\nh5 {\n  font-size: 1.3rem; }\nh6 {\n  font-size: 1rem; }\np {\n  color: #808080;\n  font-family: \"Asar\", serif; }\nul {\n  list-style-position: inside; }\nol, ul {\n  padding-left: 0;\n  margin-bottom: 0; }\nhr {\n  height: 2px;\n  border: 0 none;\n  margin: 0 auto;\n  max-width: 1200px; }\n*:focus {\n  outline: none; }\ntextarea:focus, input:focus {\n  outline: none; }\nbody {\n  overflow-x: hidden; }\nimg {\n  vertical-align: middle;\n  border-style: none;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jaHJpc3RvcGhlcm5kZS9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9wcm9qZWN0cy90cmFpbmluZ3MvdWRlbXkvamVua2lucy9hZW0tZ3VpZGVzLXdrbmQtZXZlbnRzL2FuZ3VsYXItYXBwL3NyYy9zdHlsZXMuc2NzcyIsIi9Vc2Vycy9jaHJpc3RvcGhlcm5kZS9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9wcm9qZWN0cy90cmFpbmluZ3MvdWRlbXkvamVua2lucy9hZW0tZ3VpZGVzLXdrbmQtZXZlbnRzL2FuZ3VsYXItYXBwL3NyYy9zdHlsZXMvX3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2NocmlzdG9waGVybmRlL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL3Byb2plY3RzL3RyYWluaW5ncy91ZGVteS9qZW5raW5zL2FlbS1ndWlkZXMtd2tuZC1ldmVudHMvYW5ndWxhci1hcHAvc3JjL3N0eWxlcy9fbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUdBLHVGQUFZO0FBRFosdUJBQUE7QUFHQTtFQUNFLHlCQ3FCOEI7RURwQjlCLDBDQ1lnRDtFRFhoRCxTQUFTO0VBQ1QsVUFBVTtFQUVWLGVDUHdCO0VEUXhCLGdCQ1MwQjtFRFIxQixnQkFBZ0I7RUFDaEIsY0NjOEI7RURiOUIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFFckIsa0JDeUJ1QixFQUFBO0FDMUJuQjtJRmJOO01BZ0JJLGlCQ3NCZ0IsRUFBQSxFRHBCbkI7QUFFRDtFQUNFLDBDQ1BnRCxFQUFBO0FEVWxEO0VBQ0UsZUN0QndCLEVBQUE7QUR5QjFCO0VBQ0UsaUJDekIwQixFQUFBO0FENEI1QjtFQUNFLGVDNUJ3QixFQUFBO0FEK0IxQjtFQUNFLGlCQy9CMEIsRUFBQTtBRGtDNUI7RUFDRSxpQkNsQzBCLEVBQUE7QURxQzVCO0VBQ0UsZUNyQ3dCLEVBQUE7QUR3QzFCO0VBQ0UsY0N2QjhCO0VEd0I5QiwwQkNyQ2dDLEVBQUE7QUR3Q2xDO0VBQ0UsMkJBQTJCLEVBQUE7QUFHN0I7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7QUFHbEI7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGNBQWM7RUFDZCxpQkM3QmdCLEVBQUE7QURnQ2xCO0VBQ0UsYUFBYSxFQUFBO0FBR2Y7RUFDRSxhQUFhLEVBQUE7QUFHZjtFQUNFLGtCQUFrQixFQUFBO0FBR3BCO0VBQ0Usc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixXQUFXLEVBQUEiLCJmaWxlIjoic3JjL3N0eWxlcy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4vc3R5bGVzL3NoYXJlZFwiO1xuXG4vKiBHb29nbGUgRm9udCBpbXBvcnQgKi9cbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9QXNhcnxTb3VyY2UrU2FucytQcm86NDAwLDYwMCw3MDAnKTtcblxuYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcbiAgZm9udC1mYW1pbHk6ICRmb250LXNhbnM7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcblxuICBmb250LXNpemU6ICRiYXNlLWZvbnQtc2l6ZTtcbiAgZm9udC13ZWlnaHQ6ICRmb250LXdlaWdodC1saWdodDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICRjb2xvci1ibGFjaztcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgbGluZS1oZWlnaHQ6IDEuNjtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuM3B4O1xuXG4gIHBhZGRpbmctdG9wOiAkaGVhZGVyLWhlaWdodC1iaWc7XG4gIEBpbmNsdWRlIG1lZGlhKG1vYmlsZSkge1xuICAgIHBhZGRpbmctdG9wOiAkaGVhZGVyLWhlaWdodDtcbiAgfSAgXG59XG5cbmgxLCBoMiwgaDMsIGg0IHtcbiAgZm9udC1mYW1pbHk6ICRmb250LXNhbnM7XG59XG5cbmgxIHtcbiAgZm9udC1zaXplOiAgJGgxLWZvbnQtc2l6ZTtcbn1cblxuaDIge1xuICBmb250LXNpemU6ICRoMi1mb250LXNpemU7XG59XG5cbmgzIHtcbiAgZm9udC1zaXplOiAkaDMtZm9udC1zaXplO1xufVxuXG5oNCB7XG4gIGZvbnQtc2l6ZTogJGg0LWZvbnQtc2l6ZTtcbn1cblxuaDUge1xuICBmb250LXNpemU6ICRoNS1mb250LXNpemU7XG59XG5cbmg2IHtcbiAgZm9udC1zaXplOiAkaDYtZm9udC1zaXplO1xufVxuXG5wIHtcbiAgY29sb3I6ICRjb2xvci10ZXh0O1xuICBmb250LWZhbWlseTogJGZvbnQtc2VyaWY7XG59XG5cbnVsIHtcbiAgbGlzdC1zdHlsZS1wb3NpdGlvbjogaW5zaWRlO1xufVxuXG5vbCwgdWwge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbmhyIHtcbiAgaGVpZ2h0OiAycHg7XG4gIGJvcmRlcjogMCBub25lO1xuICBtYXJnaW46IDAgYXV0bztcbiAgbWF4LXdpZHRoOiAkbWF4LXdpZHRoO1xufVxuXG4qOmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxudGV4dGFyZWE6Zm9jdXMsIGlucHV0OmZvY3Vze1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5ib2R5IHtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xufVxuXG5pbWcge1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBib3JkZXItc3R5bGU6IG5vbmU7XG4gIHdpZHRoOiAxMDAlO1xufSIsIi8vdmFyaWFibGVzIGZvciBXS05EIEV2ZW50c1xuXG4vL1R5cG9ncmFwaHlcbiRlbS1iYXNlOiAgICAgICAgICAgICAyMHB4O1xuJGJhc2UtZm9udC1zaXplOiAgICAgIDFyZW07XG4kc21hbGwtZm9udC1zaXplOiAgICAgMS40cmVtO1xuJGxlYWQtZm9udC1zaXplOiAgICAgIDJyZW07XG4kdGl0bGUtZm9udC1zaXplOiAgICAgNS4ycmVtO1xuJGgxLWZvbnQtc2l6ZTogICAgICAgIDNyZW07XG4kaDItZm9udC1zaXplOiAgICAgICAgMi41cmVtO1xuJGgzLWZvbnQtc2l6ZTogICAgICAgIDJyZW07XG4kaDQtZm9udC1zaXplOiAgICAgICAgMS41cmVtO1xuJGg1LWZvbnQtc2l6ZTogICAgICAgIDEuM3JlbTtcbiRoNi1mb250LXNpemU6ICAgICAgICAxcmVtO1xuJGJhc2UtbGluZS1oZWlnaHQ6ICAgIDEuNTtcbiRoZWFkaW5nLWxpbmUtaGVpZ2h0OiAxLjM7XG4kbGVhZC1saW5lLWhlaWdodDogICAgMS43O1xuXG4kZm9udC1zZXJpZjogICAgICAgICAnQXNhcicsIHNlcmlmO1xuJGZvbnQtc2FuczogICAgICAgICAgJ1NvdXJjZSBTYW5zIFBybycsIHNhbnMtc2VyaWY7XG5cbiRmb250LXdlaWdodC1saWdodDogICAgICAzMDA7XG4kZm9udC13ZWlnaHQtbm9ybWFsOiAgICAgNDAwO1xuJGZvbnQtd2VpZ2h0LXNlbWktYm9sZDogIDYwMDtcbiRmb250LXdlaWdodC1ib2xkOiAgICAgICA3MDA7XG5cbi8vQ29sb3JzXG4kY29sb3Itd2hpdGU6ICAgICAgICAgICAgI2ZmZmZmZjtcbiRjb2xvci1ibGFjazogICAgICAgICAgICAjMDgwODA4O1xuXG4kY29sb3IteWVsbG93OiAgICAgICAgICAgI0ZGRUEwODtcbiRjb2xvci1ncmF5OiAgICAgICAgICAgICAjODA4MDgwO1xuJGNvbG9yLWRhcmstZ3JheTogICAgICAgICM3MDcwNzA7XG5cbi8vRnVuY3Rpb25hbCBDb2xvcnNcblxuJGNvbG9yLXByaW1hcnk6ICAgICAgICAgICRjb2xvci15ZWxsb3c7XG4kY29sb3Itc2Vjb25kYXJ5OiAgICAgICAgJGNvbG9yLWdyYXk7XG4kY29sb3ItdGV4dDogICAgICAgICAgICAgJGNvbG9yLWdyYXk7XG5cblxuLy9MYXlvdXRcbiRtYXgtd2lkdGg6IDEyMDBweDtcbiRoZWFkZXItaGVpZ2h0OiA4MHB4O1xuJGhlYWRlci1oZWlnaHQtYmlnOiAxMDBweDtcblxuLy8gU3BhY2luZ1xuJGd1dHRlci1wYWRkaW5nOiAxMnB4O1xuXG4vLyBNb2JpbGUgQnJlYWtwb2ludHNcbiRtb2JpbGUtc2NyZWVuOiAxNjBweDtcbiRzbWFsbC1zY3JlZW46ICA3NjdweDtcbiRtZWRpdW0tc2NyZWVuOiA5OTJweDsiLCJAaW1wb3J0IFwidmFyaWFibGVzXCI7XG5cbkBtaXhpbiBtZWRpYSgkdHlwZXMuLi4pIHtcbiAgQGVhY2ggJHR5cGUgaW4gJHR5cGVzIHtcblxuICAgIEBpZiAkdHlwZSA9PSB0YWJsZXQge1xuICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAkc21hbGwtc2NyZWVuICsgMSkgYW5kIChtYXgtd2lkdGg6ICRtZWRpdW0tc2NyZWVuKSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cblxuICAgIEBpZiAkdHlwZSA9PSBkZXNrdG9wIHtcbiAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogJG1lZGl1bS1zY3JlZW4gKyAxKSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cblxuICAgIEBpZiAkdHlwZSA9PSBtb2JpbGUge1xuICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAkbW9iaWxlLXNjcmVlbiArIDEpIGFuZCAobWF4LXdpZHRoOiAkc21hbGwtc2NyZWVuKSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gY29udGVudC1hcmVhICgpIHtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1heC13aWR0aDogJG1heC13aWR0aDtcbiAgcGFkZGluZzogJGd1dHRlci1wYWRkaW5nO1xufVxuXG5AbWl4aW4gY29tcG9uZW50LXBhZGRpbmcoKSB7XG4gIHBhZGRpbmc6IDAgJGd1dHRlci1wYWRkaW5nICFpbXBvcnRhbnQ7XG59XG5cbkBtaXhpbiBkcm9wLXNoYWRvdyAoKSB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59Il19 */", '', '']]

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/styles.scss":
/*!*************************!*\
  !*** ./src/styles.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../node_modules/postcss-loader/src??embedded!../node_modules/sass-loader/lib/loader.js??ref--14-3!./styles.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/styles.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!*******************************!*\
  !*** multi ./src/styles.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/christophernde/Library/Mobile Documents/com~apple~CloudDocs/projects/trainings/udemy/jenkins/aem-guides-wknd-events/angular-app/src/styles.scss */"./src/styles.scss");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map